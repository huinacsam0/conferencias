<?php

include_once "interfaces/interUsuario.php";
include_once "conexiones/validar.php";

final class UsuarioClass extends Validar implements Usuario
{
    private $nombres    = "";
    private $direccion  = "";
    private $user       = "";
    private $cone;

    function __construct() {
        $this->cone  = new Conectar();   
    }

    public function setNombres($name)
    {
        $this->nombres = $name;
    }
    public function setDireccion($direccion)
    {
        $this->direccion = $direccion;
    }
    public function setUsuario($user,$contraseña)
    {
        $this->user = $user;
    }

    public function getDatos(){
        
        return $this->cone->queryLst("SELECT * FROM conferencias_db.vstUsuarios;");
    }

    private function validarElementos($data){

        $res = array();
        foreach (json_decode($data) as $key => $value) {
            $res[$key] = "'".$value."'";
        }
        return (object)$res;
    }

    public function registroUsuario($datos){
        
        $this->validarDatos($datos,"accion");
        $this->validarDatos($datos,"nombres", array("tipo"=> "text",'requerido' => true));
        $this->validarDatos($datos,"apellidos",array("tipo"=> "text",'requerido' => true));
        $this->validarDatos($datos,"telefono");
        $this->validarDatos($datos,"direccion");
        $this->validarDatos($datos,"usuario",array("tipo"=> "text",'requerido' => true));
        $this->validarDatos($datos,"correo",array("tipo"=> "text",'requerido' => true));
        $this->validarDatos($datos,"contrasena",array("tipo"=> "text",'requerido' => true));

        $sql = "CALL prGuardarUsuario($datos->accion, $datos->nombres, $datos->apellidos, $datos->telefono, $datos->direccion, $datos->usuario, $datos->correo, $datos->contrasena);";
    
        return $this->cone->queryCRUD($sql);
    }

    public function login($datos){

        $this->validarDatos($datos,"usuario",array("tipo"=> "text",'requerido' => true));
        $this->validarDatos($datos,"contrasena",array("tipo"=> "text",'requerido' => true));
        $sql = "CALL conferencias_db.prLogin($datos->usuario,$datos->contrasena);";

        $rs =  $this->cone->queryCRUD($sql,false);
        if($rs['respuesta'] == 1){
            $rs['token'] = $this->generateToken();
        }
        return jsRespuesta($rs); 
    }


}
