<?php


error_reporting(E_ALL);
ini_set('display_errors', '1');
header('Content-Type: application/json');
header('Content-type: application/x-www-form-urlencoded');
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
header("Allow: GET, POST, OPTIONS, PUT, DELETE");

include_once 'autoload.php';     
include_once 'conexiones/conectar.php';

$metodos = array("GET", "POST", "PUT", "DELETE");

if(in_array($_SERVER["REQUEST_METHOD"],$metodos)){

    if(isset($_POST['registroUser'])){
        
        $data = $_POST['registroUser'];
        $user = new UsuarioClass();
        $user->registroUsuario($data);
    
    }else if(isset($_GET['usuarios'])){
        $user = new UsuarioClass();
        $user->getDatos();
    }else if (isset($_POST['login'])){
        $data = $_POST['login'];
        $user = new UsuarioClass();
        $user->login(json_decode($data));
    }
    else{

        return jsRespuesta(array(
            "mensaje" => "Ingrese el metodo y los parametros necesarios, verifique no existe el metodo",
        ));
    }
}
