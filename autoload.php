<?php
    
    function miAutoload() {
        spl_autoload_register(function ($class) {
            $file =  'clases_de_uso/'.str_replace('\\', DIRECTORY_SEPARATOR, $class).'.php';
            
            if (file_exists($file)) {
                require_once $file;
                return true;
            }
            return false;
        });
    }

    miAutoload();
?>