<?php

    interface Conferencia
    {
        public function setNombre($name);
        public function setFecha($fecha);
        public function setHora($hora);
        public function setDuration($duration);
        public function getConferencias();
    }

?>