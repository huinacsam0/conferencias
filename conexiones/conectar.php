<?php

    include "fnGen.php";
    final class Conectar
    {
        private $user = 'user_uno';
        private $pass = 'oro12345';
        private $dsn = 'mysql:host=localhost;dbname=conferencias_db';


        private function query($sql) : array {
            try {
                // Crear una nueva instancia de PDO
                $conexion = new PDO($this->dsn, $this->user, $this->pass);
            
                // Establecer el modo de error de PDO a excepción
                $conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
                // Preparar la consulta
                $statement = $conexion->prepare($sql);

                // Ejecutar la consulta
                $statement->execute();
                
                // Obtener los resultados como un array asociativo
                return  $statement->fetchAll(PDO::FETCH_ASSOC);
                
            } catch (PDOException $e) {
                return array("error" => $e->getMessage()); 
            }
        }
        public function queryLst($sql) { 

            $lstElementos = $this->query($sql);
            $rs = [];

            if(count($lstElementos) > 0){
                $rs = $this->respuestaMensajes($lstElementos, "Consulta realizada exitosamente", "1");
            }else{
                $rs = $this->respuestaMensajes($lstElementos);
            }
            return jsRespuesta($rs);
        }

        public function queryCRUD( $sql, bool $json = true){

            $rsCRUD = $this->query($sql);
            $rs = [];

            if(array_key_exists("error",$rsCRUD)){
                $rs = $this->respuestaMensajes([],$rsCRUD["error"]);
            }else{
                $rs = $rsCRUD[0];
            }

            return $json ? jsRespuesta($rs) : $rs;
        }

        private function respuestaMensajes($data, $mensaje = "No hay datos", $respuesta = "0"){
            return array(
                "mensaje" => $mensaje,
                "respuesta" => $respuesta,
                "datos" => $data
            );
        }



    }
    