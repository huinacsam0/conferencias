<?php

    class Validar 
    {
        public function validarDatos(&$datos, String $propiedad, $opciones = array("tipo"=> "text", "requerido" => false, "ausencia" => "")){

            if(gettype($datos) == "string"){$datos = json_decode($datos);}

            $opcion = (object)$opciones;
            switch ($opcion->tipo) {
                case 'text':
                    if(empty($datos->$propiedad))
                    {
                        if($opcion->requerido){
                            http_response_code(402);
                            jsRespuesta(array("respuesta" => "warning", "mensaje" => "Requerido la propiedad $propiedad")); 
                            exit();
                        }
                        $datos->$propiedad = "'".$opcion->ausencia."'";

                    }else{
                        $this->limpiarTexto($datos->$propiedad);
                        $datos->$propiedad = "'{$datos->$propiedad}'";
                    }
                    break;
                
                default:
                    break;
            }
            /*if (empty($name)) {
                echo "Name is required";
            } else if (!preg_match("/^[a-zA-Z-' ]*$/", $name)) {
                echo "Only letters and white space allowed";
            }
        
            if (empty($email)) {
                echo "Email is required";
            } else if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                echo "Invalid email format";
            }
        
            if (!empty($website) && !filter_var($website, FILTER_VALIDATE_URL)) {
                echo "Invalid URL";
            }
        
            if (empty($gender)) {
                echo "Gender is required";
            }*/
        }
    
        private function limpiarTexto(&$data) {
            $data = trim($data);
            $data = stripslashes($data);
            $data = htmlspecialchars($data);
            return $data;
        }

        public function generateToken($length = 32) {
            return bin2hex(random_bytes($length));
        }
    }
    